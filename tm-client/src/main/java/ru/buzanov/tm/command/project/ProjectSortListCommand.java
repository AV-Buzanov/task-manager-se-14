package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Exception_Exception;
import ru.buzanov.tm.endpoint.Field;
import ru.buzanov.tm.endpoint.Project;
import ru.buzanov.tm.endpoint.Session;

import java.util.*;

public class ProjectSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects sorted";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[SORTED PROJECT LIST]");
        @NotNull final Session session = serviceLocator.getCurrentSession();
        @NotNull List<Project> list = new ArrayList<>();
        terminalService.printLineG("[SORT BY]");
        terminalService.printLine("1 : By creation");
        terminalService.printLine("2 : By name");
        terminalService.printLine("3 : By start date");
        terminalService.printLine("4 : By end date");
        terminalService.printLine("5 : By tasks");
        terminalService.printLine("6 : By status");
        @NotNull final String comp = terminalService.readLine();
        terminalService.printLineG("[DIRECTION]");
        terminalService.printLine("1 : Rising");
        terminalService.printLine("2 : Falling");
        boolean dir = true;
        if ("2".equals(terminalService.readLine()))
            dir = false;
        switch (comp) {
            case ("1"):
                list = projectService.findAllP(session);
                if (!dir)
                    Collections.reverse(list);
                break;
            case ("2"):
                list = projectService.findAllOrderedP(session, dir, Field.NAME);
                break;
            case ("3"):
                list = projectService.findAllOrderedP(session, dir, Field.START_DATE);
                break;
            case ("4"):
                list = projectService.findAllOrderedP(session, dir, Field.FINISH_DATE);
                break;
            case ("5"):
                list = projectService.findAllP(session);
                if (dir)
                    list.sort(new Comparator<Project>() {
                        @Override
                        public int compare(Project o1, Project o2) {
                            int o1size = 0;
                            try {
                                o1size = Objects.requireNonNull(taskService.findByProjectIdT(serviceLocator.getCurrentSession(), o1.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            int o2size = 0;
                            try {
                                o2size = Objects.requireNonNull(taskService.findByProjectIdT(serviceLocator.getCurrentSession(), o2.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            return Integer.compare(o1size, o2size);
                        }
                    });
                else
                    list.sort(new Comparator<Project>() {
                        @Override
                        public int compare(Project o1, Project o2) {
                            int o1size = 0;
                            try {
                                o1size = Objects.requireNonNull(taskService.findByProjectIdT(serviceLocator.getCurrentSession(), o1.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            int o2size = 0;
                            try {
                                o2size = Objects.requireNonNull(taskService.findByProjectIdT(serviceLocator.getCurrentSession(), o2.getId())).size();
                            } catch (Exception_Exception e) {
                                e.printStackTrace();
                            }
                            return Integer.compare(o1size, o2size) * (-1);
                        }
                    });
                break;
            case ("6"):
                list = projectService.findAllP(session);
                list.sort(terminalService.getStatusComparator(dir));
                break;
            default:
                break;
        }
        for (@NotNull final Project project : list) {
            terminalService.printWBS(project);
            terminalService.printLine();
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
