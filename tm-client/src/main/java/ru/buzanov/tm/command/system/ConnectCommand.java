package ru.buzanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.Exception;

import java.io.IOException;

public class ConnectCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "connect";
    }

    @Override
    public @NotNull String description() {
        return "Connect to server.";
    }

    @Override
    public void execute() throws Exception, IOException {
        serviceLocator.connect();
    }

    @Override
    public boolean isSecure() throws Exception, java.lang.Exception {
        return false;
    }
}
