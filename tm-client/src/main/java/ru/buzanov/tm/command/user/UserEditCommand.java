package ru.buzanov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.User;

public class UserEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit user data";
    }

    @Override
    public void execute() throws Exception {
        terminalService.printLineG("[EDIT USER DATA]");
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull String stringBuf;

        terminalService.printLineG("[LOGIN]");
        terminalService.printLine(user.getLogin());
        terminalService.printLineG("[ENTER NEW LOGIN]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()) {
            if (userService.isLoginExist(stringBuf)) {
                terminalService.printLine("User with this login already exist.");
                return;
            }
            user.setLogin(stringBuf);
        }
        terminalService.printLineG("[NAME]");
        terminalService.printLine(user.getName());
        terminalService.printLineG("[ENTER NEW NAME]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()) {
            user.setName(stringBuf);
        }
        terminalService.printLineG("[ENTER NEW PASS IF NECESSARY]");
        stringBuf = terminalService.readLine();
        if (!stringBuf.isEmpty()) {
            if (stringBuf.length() < 6)
                terminalService.printLine("Pass can't be less then 6 symbols");
            else {
                terminalService.printLineG("[REPEAT NEW PASS]");
                if (stringBuf.equals(terminalService.readLine())) {
                    user.setPasswordHash(stringBuf);
                } else
                    terminalService.printLine("Pass not match");
            }
        }
        userService.merge(serviceLocator.getCurrentSession(), user);
        serviceLocator.setCurrentUser(userService.findOne(serviceLocator.getCurrentSession()));
        terminalService.printLineG("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
