package ru.buzanov.tm.constant;

import java.io.File;

public class FormatConst {
    public final static String DATE_FORMAT = "dd.MM.yyyy";
    public final static String EMPTY_FIELD = "-----";
    public final static String SAVE_PATH = System.getProperty("user.dir")+ File.separator+"taskmanager"+File.separator;
}