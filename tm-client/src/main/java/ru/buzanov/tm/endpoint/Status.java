
package ru.buzanov.tm.endpoint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for status.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="status"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Запланированно"/&gt;
 *     &lt;enumeration value="В процессе"/&gt;
 *     &lt;enumeration value="Завершено"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "status")
@XmlEnum
public enum Status {

    @XmlEnumValue("\u0417\u0430\u043f\u043b\u0430\u043d\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u043e")
    ЗАПЛАНИРОВАННО("\u0417\u0430\u043f\u043b\u0430\u043d\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u043e"),
    @XmlEnumValue("\u0412 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u0435")
    В_ПРОЦЕССЕ("\u0412 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u0435"),
    @XmlEnumValue("\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043e")
    ЗАВЕРШЕНО("\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043e");
    private final String value;

    Status(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Status fromValue(String v) {
        for (Status c: Status.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
