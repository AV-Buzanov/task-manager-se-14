package ru.buzanov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.util.PropertyService;

import javax.persistence.EntityManagerFactory;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

    @NotNull ISessionService getSessionService();

    @NotNull PropertyService getPropertyService();

    @NotNull EntityManagerFactory getManagerFactory();
}
