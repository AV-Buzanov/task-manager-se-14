package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.Field;

import java.sql.SQLException;
import java.util.Collection;

public interface IWBSRepository<T> extends IRepository<T> {

    @NotNull Collection<T> findAll(@NotNull final String userId) throws Exception;

    @Nullable T findOne(@NotNull final String userId, @NotNull final String id) throws Exception;

    void merge(@NotNull final String userId, @NotNull final T project) throws Exception;

    void remove(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAll(@NotNull final String userId) throws Exception;

    @NotNull Collection<T> findByDescription(@NotNull final String userId, @NotNull final String desc) throws Exception;

    @NotNull Collection<T> findByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    @Nullable T findName(@NotNull final String userId, @NotNull final String name) throws Exception;

    @NotNull Collection<T> findAllOrdered(@NotNull String userId, boolean dir, @NotNull Field field) throws Exception;

}
