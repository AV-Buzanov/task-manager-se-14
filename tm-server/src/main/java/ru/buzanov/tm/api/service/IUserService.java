package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserService extends IService<UserDTO> {

    @Nullable UserDTO findByLogin(@Nullable String login) throws Exception;

    boolean isPassCorrect(@Nullable final String login, @Nullable final String pass) throws Exception;

    boolean isLoginExist(@Nullable final String login) throws Exception;

    @Nullable Collection<UserDTO> findByRole(@Nullable final RoleType role) throws Exception;
}
