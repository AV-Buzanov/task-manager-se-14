package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.SessionService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;
import ru.buzanov.tm.util.PropertyService;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final IProjectService projectService = new ProjectService(this);
    @NotNull
    private final ITaskService taskService = new TaskService(this);
    @NotNull
    private final IUserService userService = new UserService(this);
    @NotNull
    private final ISessionService sessionService = new SessionService(this);
    @NotNull
    private final PropertyService propertyService = new PropertyService();
    @NotNull
    private final EntityManagerFactory managerFactory = getEntityManagerFactory();
    @NotNull
    private final String adress = "http://0.0.0.0:8080/";

    public void start() {
        Thread sessionCleanThread = new SessionCleanThread(this);
        sessionCleanThread.setDaemon(true);
        sessionCleanThread.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Endpoint.publish(adress + "ProjectEndpoint?wsdl", new ProjectEndpoint(this));
        Endpoint.publish(adress + "TaskEndpoint?wsdl", new TaskEndpoint(this));
        Endpoint.publish(adress + "AdminUserEndpoint?wsdl", new AdminUserEndpoint(this));
        Endpoint.publish(adress + "SessionEndpoint?wsdl", new SessionEndpoint(this));
        Endpoint.publish(adress + "UserEndpoint?wsdl", new UserEndpoint(this));
        System.out.println("TaskDTO manager server is running.");
        System.out.println(adress + "ProjectEndpoint?wsdl");
        System.out.println(adress + "TaskEndpoint?wsdl");
        System.out.println(adress + "AdminUserEndpoint?wsdl");
        System.out.println(adress + "SessionEndpoint?wsdl");
        System.out.println(adress + "UserEndpoint?wsdl");

        while (true) {
            try {
                if ("exit".equals(reader.readLine())) {
                    System.exit(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public EntityManagerFactory getEntityManagerFactory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(Environment.URL, propertyService.getJdbcUrl2());
        settings.put(Environment.USER, propertyService.getJdbcUsername());
        settings.put(Environment.PASS, propertyService.getJdbcPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}