package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint {

    public AdminUserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public UserDTO findByLogin(@Nullable SessionDTO session, @Nullable String login) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findByLogin(login);
    }

    @WebMethod
    @Nullable
    public Collection<UserDTO> findByRole(@Nullable SessionDTO session, @Nullable RoleType role) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findByRole(role);
    }

    @WebMethod
    @NotNull
    public Collection<UserDTO> findAll(@Nullable SessionDTO session) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findAll();
    }

    @WebMethod
    @Nullable
    public UserDTO findOneAdmin(@Nullable SessionDTO session, @Nullable String id) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        return userService.findOne(id);
    }

    @WebMethod
    @Nullable
    public UserDTO removeAdmin(@Nullable SessionDTO session, @Nullable String id) throws Exception {
        auth(session);
        if (!userService.findOne(session.getUserId()).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("This command for admin only.");
        if (userService.findOne(id).getRoleType().equals(RoleType.ADMIN))
            throw new Exception("You can't removeA admin.");
        userService.remove(id);
        return null;
    }
}
