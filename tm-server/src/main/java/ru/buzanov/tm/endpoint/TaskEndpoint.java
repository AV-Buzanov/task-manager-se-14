package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Field;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint {

    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public Collection<TaskDTO> findByProjectIdT(@Nullable SessionDTO session, @Nullable String projectId) throws Exception {
        auth(session);
        return taskService.findByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void removeByProjectIdT(@Nullable SessionDTO session, @Nullable String projectId) throws Exception {
        auth(session);
        taskService.removeByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    @NotNull
    public Collection<TaskDTO> findAllT(@Nullable SessionDTO session) throws Exception {
        auth(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    @NotNull
    public Collection<TaskDTO> findAllOrderedT(@Nullable SessionDTO session, boolean dir, @NotNull Field field) throws Exception {
        auth(session);
        return taskService.findAllOrdered(session.getUserId(), dir, field);
    }

    @WebMethod
    @Nullable
    public TaskDTO findOneT(@Nullable SessionDTO session, @Nullable String id) throws Exception {
        auth(session);
        return taskService.findOne(session.getUserId(), id);
    }

    @WebMethod
    public boolean isNameExistT(@Nullable SessionDTO session, @Nullable String name) throws Exception {
        auth(session);
        return taskService.isNameExist(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public String getListT(@Nullable SessionDTO session) throws Exception {
        auth(session);
        return taskService.getList(session.getUserId());
    }

    @WebMethod
    @Nullable
    public String getIdByCountT(@Nullable SessionDTO session, int count) throws Exception {
        auth(session);
        return taskService.getIdByCount(session.getUserId(), count);
    }

    @WebMethod
    public void mergeT(@Nullable SessionDTO session, @Nullable String id, @Nullable TaskDTO project) throws Exception {
        auth(session);
        taskService.merge(session.getUserId(), id, project);
    }

    @WebMethod
    @Nullable
    public TaskDTO removeT(@Nullable SessionDTO session, @Nullable String id) throws Exception {
        auth(session);
        taskService.remove(session.getUserId(), id);
        return null;
    }

    @WebMethod
    public void removeAllT(@Nullable SessionDTO session) throws Exception {
        auth(session);
        taskService.removeAll(session.getUserId());
    }

    @WebMethod
    @NotNull
    public Collection<TaskDTO> findByDescriptionT(@Nullable SessionDTO session, @Nullable String desc) throws Exception {
        auth(session);
        return taskService.findByDescription(session.getUserId(), desc);
    }

    @WebMethod
    @NotNull
    public Collection<TaskDTO> findByNameT(@Nullable SessionDTO session, @Nullable String name) throws Exception {
        auth(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public TaskDTO loadT(@Nullable SessionDTO session, @Nullable TaskDTO entity) throws Exception {
        taskService.load(session.getUserId(), entity);
        return null;
    }

    @WebMethod
    public void loadListT(@Nullable SessionDTO session, List<TaskDTO> list) throws Exception {
        taskService.load(session.getUserId(), list);
    }
}
