package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;

import javax.persistence.TypedQuery;
import java.util.List;

public abstract class AbstractRepository {

    protected <T> T getEntity(@NotNull final TypedQuery<T> query) {
        @NotNull final List<T> results = query.getResultList();
        if (results.isEmpty())
            return null;
        return results.get(0);
    }
}
