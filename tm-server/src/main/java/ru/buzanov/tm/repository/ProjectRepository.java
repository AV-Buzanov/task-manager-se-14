package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.enumerated.Field;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class ProjectRepository extends AbstractRepository implements IProjectRepository {
    @NotNull
    private final EntityManager manager;

    public ProjectRepository(@NotNull final EntityManager manager) {
        this.manager = manager;
    }

    public void load(@NotNull final Project project) {
        manager.persist(project);
    }

    public void load(@NotNull final List<Project> list) {
        for (@NotNull final Project project : list) {
            load(project);
        }
    }

    public void merge(@NotNull final Project project) {
        manager.merge(project);
    }

    public void merge(@NotNull final String userId, @NotNull final Project project) {
        manager.merge(project);
    }

    @Nullable
    public Project findOne(@NotNull final String id) {
        return manager.find(Project.class, id);
    }

    @Nullable
    public Project findOne(@NotNull final String userId, @NotNull final String id) {
        return getEntity(manager.createQuery("SELECT s FROM Project s WHERE s.user.id=:userId and s.id = :id", Project.class).
                setParameter("userId", userId).setParameter("id", id));
    }

    public @NotNull Collection<Project> findAll() {
        return manager.createQuery("SELECT s FROM Project s", Project.class).getResultList();
    }

    public @NotNull Collection<Project> findAll(@NotNull final String userId) {
        return manager.createQuery("SELECT s FROM Project s WHERE s.user.id=:userId", Project.class).
                setParameter("userId", userId).getResultList();
    }

    public @NotNull Collection<Project> findAllOrdered(@NotNull final String userId, boolean dir, @NotNull final Field field) {
        @NotNull String orderDir = "";
        if (dir) orderDir = "DESC";
        return manager.createQuery("SELECT s FROM Project s WHERE s.user.id=:userId ORDER BY :field :dir", Project.class).
                setParameter("userId", userId).setParameter("field", field.displayName()).setParameter("dir", orderDir).getResultList();
    }

    public @NotNull Collection<Project> findByName(@NotNull final String userId, @NotNull final String name) {
        return manager.createQuery("SELECT s FROM Project s WHERE s.user.id=:userId and s.name=:name", Project.class).setParameter("name", name).
                setParameter("userId", userId).getResultList();
    }

    @Nullable
    public Project findName(@NotNull final String userId, @NotNull final String name) {
        return getEntity(manager.createQuery("SELECT s FROM Project s WHERE s.user.id=:userId and s.name=:name", Project.class).
                setParameter("userId", userId).setParameter("name", name));
    }

    public @NotNull Collection<Project> findByDescription(@NotNull final String userId, @NotNull final String desc) {
        return manager.createQuery("SELECT s FROM Project s WHERE s.user.id=:userId and s.description=:desc", Project.class).setParameter("desc", desc).
                setParameter("userId", userId).getResultList();
    }

    public void remove(@NotNull final String id) throws Exception {
        manager.remove(manager.find(Project.class, id));
    }

    public void remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        manager.remove(findOne(userId, id));
    }

    public void removeAll() throws Exception {
        for (Project project : findAll())
            manager.remove(project);
    }

    public void removeAll(@NotNull final String userId) throws Exception {
        for (Project project : findAll(userId))
            manager.remove(project);
    }
}