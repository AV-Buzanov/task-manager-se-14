package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class SessionRepository extends AbstractRepository implements ISessionRepository {
    @NotNull
    private final EntityManager manager;

    public SessionRepository(@NotNull final EntityManager manager) {
        this.manager = manager;
    }

    public void load(@NotNull final Session session) {
        manager.persist(session);
    }

    public void load(@NotNull final List<Session> list) {
        for (@NotNull final Session session : list) {
            load(session);
        }
    }

    public void merge(@NotNull final Session session) {
        manager.merge(session);
    }

    public void merge(@NotNull final String userId, @NotNull final Session session) {
        if (session.getUser() == null || !userId.equals(session.getUser().getId()))
            return;
        manager.merge(session);
    }

    @Nullable
    public Session findOne(@NotNull final String id) {
        return manager.find(Session.class, id);
    }

    @Nullable
    public Session findOne(@NotNull final String userId, @NotNull final String id) {
        return getEntity(manager.createQuery("SELECT s FROM Session s WHERE s.user.id=:userId and s.id = :id", Session.class).
                setParameter("userId", userId).setParameter("id", id));
    }

    public @NotNull Collection<Session> findAll() {
        return manager.createQuery("SELECT s FROM Session s", Session.class).getResultList();
    }

    public @NotNull Collection<Session> findAll(@NotNull final String userId) {
        return manager.createQuery("SELECT s FROM Session s WHERE s.user.id=:userId", Session.class).
                setParameter("userId", userId).getResultList();
    }

    public void remove(@NotNull final String id) {
        manager.remove(manager.find(Session.class, id));
    }

    public void remove(@NotNull final String userId, @NotNull final String id) {
        manager.remove(findOne(userId, id));
    }

    public void removeAll() {
        manager.createQuery("DELETE FROM session").executeUpdate();
    }

    public void removeAll(@NotNull final String userId) {
        manager.createQuery("DELETE FROM Session s WHERE s.user.id=:userId").setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public Collection<Session> findByUserId(@NotNull final String userId) {
        return manager.createQuery("SELECT s FROM Session s WHERE s.user.id=:userId ", Session.class).
                setParameter("userId", userId).getResultList();
    }
}