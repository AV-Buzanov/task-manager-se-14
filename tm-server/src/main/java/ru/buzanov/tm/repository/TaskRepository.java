package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Field;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class TaskRepository extends AbstractRepository implements ITaskRepository {
    @NotNull
    private final EntityManager manager;

    public TaskRepository(@NotNull final EntityManager manager) {
        this.manager = manager;
    }

    public void load(@NotNull final Task task) {
        manager.persist(task);
    }

    public void load(@NotNull final List<Task> list) {
        for (@NotNull final Task task : list) {
            load(task);
        }
    }

    public void merge(@NotNull final Task task) {
        manager.merge(task);
    }

    public void merge(@NotNull final String userId, @NotNull final Task task) {
        if (task.getUser() == null || !userId.equals(task.getUser().getId()))
            return;
        manager.merge(task);
    }

    @Nullable
    public Task findOne(@NotNull String id) {
        return manager.find(Task.class, id);
    }

    @Nullable
    public Task findOne(@NotNull String userId, @NotNull String id) {
        return getEntity(manager.createQuery("SELECT s FROM Task s WHERE s.user.id=:userId and s.id = :id", Task.class).
                setParameter("userId", userId).setParameter("id", id));
    }

    public @NotNull Collection<Task> findAll() {
        return manager.createQuery("SELECT s FROM Task s", Task.class).getResultList();
    }

    public @NotNull Collection<Task> findAll(@NotNull String userId) {
        return manager.createQuery("SELECT s FROM Task s WHERE s.user.id=:userId", Task.class).
                setParameter("userId", userId).getResultList();
    }

    public @NotNull Collection<Task> findAllOrdered(@NotNull String userId, boolean dir, @NotNull Field field) {
        String orderDir = "";
        if (dir) orderDir = "DESC";
        return manager.createQuery("SELECT s FROM Task s WHERE s.user.id=:userId ORDER BY :field :dir", Task.class).
                setParameter("userId", userId).setParameter("field", field.displayName()).setParameter("dir", orderDir).getResultList();
    }

    public @NotNull Collection<Task> findByName(@NotNull String userId, @NotNull String name) {
        return manager.createQuery("SELECT s FROM Task s WHERE s.userId=:user.id and s.name=:name", Task.class).setParameter("name", name).
                setParameter("userId", userId).getResultList();
    }

    @Nullable
    public Task findName(@NotNull String userId, @NotNull String name) {
        return getEntity(manager.createQuery("SELECT s FROM Task s WHERE s.user.id=:userId and s.name=:name", Task.class).
                setParameter("userId", userId).setParameter("name", name));
    }

    public @NotNull Collection<Task> findByDescription(@NotNull String userId, @NotNull String desc) {
        return manager.createQuery("SELECT s FROM Task s WHERE s.user.id=:userId and s.description=:desc", Task.class).setParameter("desc", desc).
                setParameter("userId", userId).getResultList();
    }

    public void remove(@NotNull String id) {
        manager.remove(manager.find(Task.class, id));
    }

    public void remove(@NotNull String userId, @NotNull String id) {
        manager.remove(findOne(userId, id));
    }

    public void removeAll() {
        for (Task task : findAll())
            manager.remove(task);
    }

    public void removeAll(@NotNull String userId) {
        for (Task task : findAll(userId))
            manager.remove(task);
    }

    @NotNull
    public Collection<Task> findByProjectId(@NotNull final String projectId) {
        return manager.createQuery("SELECT s FROM Task s WHERE s.project.id=:projectId", Task.class).
                setParameter("projectId", projectId).getResultList();
    }

    @NotNull
    public Collection<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return manager.createQuery("SELECT s FROM Task s WHERE s.user.id=:userId and s.project.id=:projectId", Task.class).setParameter("projectId", projectId).
                setParameter("userId", userId).getResultList();
    }


    public void removeByProjectId(@NotNull final String projectId) {
        manager.remove(findByProjectId(projectId));
    }

    public void removeByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        manager.remove(findByProjectId(userId, projectId));
    }
}