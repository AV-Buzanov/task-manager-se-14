package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class UserRepository extends AbstractRepository implements IUserRepository {
    @NotNull
    private final EntityManager manager;

    public UserRepository(@NotNull final EntityManager manager) {
        this.manager = manager;
    }

    public void load(@NotNull final User user) {
        manager.persist(user);
    }

    public void load(@NotNull final List<User> list) {
        for (@NotNull final User user : list) {
            load(user);
        }
    }

    public void merge(@NotNull final User user) {
        manager.merge(user);
    }

    @Nullable
    public User findOne(@NotNull final String id) {
        return manager.find(User.class, id);
    }

    public @NotNull Collection<User> findAll() {
        return manager.createQuery("SELECT s FROM User s", User.class).getResultList();
    }

    public void remove(@NotNull final String id) {
        manager.remove(manager.find(User.class, id));
    }

    public void removeAll() {
        manager.createQuery("DELETE FROM User").executeUpdate();
    }

    @Nullable
    public User findName(@NotNull final String name) {
        return getEntity(manager.createQuery("SELECT s FROM User s WHERE  s.name=:name", User.class).
                setParameter("name", name));
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        return getEntity(manager.createQuery("SELECT s FROM User s WHERE s.login=:login", User.class).
                setParameter("login", login));
    }

    @Override
    public @NotNull Collection<User> findByRole(@NotNull final RoleType role) {
        return manager.createQuery("SELECT s FROM User s WHERE s.roleType=:roleType", User.class).
                setParameter("roleType", role.displayName()).getResultList();
    }
}