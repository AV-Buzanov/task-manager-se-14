package ru.buzanov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ProjectService extends AbstractWBSService<ProjectDTO> implements IProjectService {
    @NotNull
    private IProjectRepository repository;
    @NotNull
    private EntityManager manager;

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(@Nullable final String userId, @Nullable final ProjectDTO project) throws Exception {
        if (project == null || project.getId() == null || project.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        project.setUserId(userId);
        if (project.getCreateDate() == null)
            project.setCreateDate(new Date(System.currentTimeMillis()));
        if (isNameExist(userId, project.getName()))
            throw new Exception("This name already exist!");
        openTransaction();
        repository.load(toEntity(project));
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public void load(@Nullable final String userId, @Nullable final List<ProjectDTO> list) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        for (@NotNull final ProjectDTO project : list)
            repository.load(toEntity(project));
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public @NotNull Collection<ProjectDTO> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        for (@NotNull final Project project : repository.findAll(userId))
            list.add(toDTO(project));
        manager.close();
        return list;
    }

    @Override
    public @NotNull Collection<ProjectDTO> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        for (@NotNull final Project project : repository.findByName(userId, name))
            list.add(toDTO(project));
        manager.close();
        return list;
    }

    @Override
    public @NotNull Collection<ProjectDTO> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (desc == null || desc.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        for (@NotNull final Project project : repository.findByDescription(userId, desc))
            list.add(toDTO(project));
        manager.close();
        return list;
    }

    @Nullable
    @Override
    public ProjectDTO findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            return toDTO(repository.findOne(userId, id));
        } finally {
            manager.close();
        }
    }

    @Override
    public boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            @Nullable final Project project = repository.findName(userId, name);
            return project != null;
        } finally {
            manager.close();
        }
    }

    @Override
    public void merge(@Nullable String userId, @Nullable final String id, @Nullable final ProjectDTO project) throws Exception {
        if (project == null || project.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (isNameExist(userId, project.getName()))
            throw new Exception("This name already exist!");
        project.setUserId(userId);
        openTransaction();
        repository.merge(userId, toEntity(project));
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public @NotNull Collection<ProjectDTO> findAllOrdered(@Nullable final String userId, boolean dir, @NotNull final Field field) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (field == null)
            return new ArrayList<>();
        openTransaction();
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        for (@NotNull final Project project : repository.findAllOrdered(userId, dir, field))
            list.add(toDTO(project));
        manager.close();
        return list;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            repository.remove(userId, id);
            manager.getTransaction().commit();
        } catch (Exception e) {
            throw new Exception("Something wrong.");
        } finally {
            manager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        repository.removeAll(userId);
        manager.getTransaction().commit();
        manager.close();
    }

    private void openTransaction() {
        manager = serviceLocator.getManagerFactory().createEntityManager();
        repository = new ProjectRepository(manager);
        manager.getTransaction().begin();
    }

    @Nullable
    private ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null)
            return null;
        @NotNull final ProjectDTO projectDto = new ProjectDTO();
        if (project.getId() != null)
            projectDto.setId(project.getId());
        if (project.getName() != null)
            projectDto.setName(project.getName());
        if (project.getStartDate() != null)
            projectDto.setStartDate(project.getStartDate());
        if (project.getFinishDate() != null)
            projectDto.setFinishDate(project.getFinishDate());
        if (project.getDescription() != null)
            projectDto.setDescription(project.getDescription());
        if (project.getStatus() != null)
            projectDto.setStatus(project.getStatus());
        if (project.getCreateDate() != null)
            projectDto.setCreateDate(project.getCreateDate());
        if (project.getUser() != null)
            projectDto.setUserId(project.getUser().getId());
        return projectDto;
    }

    @Nullable
    private Project toEntity(@Nullable final ProjectDTO project) {
        if (project == null)
            return null;
        @NotNull final Project projectEntity = new Project();
        if (project.getId() != null)
            projectEntity.setId(project.getId());
        if (project.getName() != null)
            projectEntity.setName(project.getName());
        if (project.getStartDate() != null)
            projectEntity.setStartDate(project.getStartDate());
        if (project.getFinishDate() != null)
            projectEntity.setFinishDate(project.getFinishDate());
        if (project.getDescription() != null)
            projectEntity.setDescription(project.getDescription());
        if (project.getStatus() != null)
            projectEntity.setStatus(project.getStatus());
        if (project.getCreateDate() != null)
            projectEntity.setCreateDate(project.getCreateDate());
        @NotNull final UserRepository userRepository = new UserRepository(manager);
        @NotNull final TaskRepository taskRepository = new TaskRepository(manager);
        if (project.getUserId() != null)
            try {
                projectEntity.setUser(userRepository.findOne(project.getUserId()));
            } catch (NoResultException e) {
                projectEntity.setUser(null);
            }
        for (@NotNull final Task task : taskRepository.findAll(project.getUserId()))
            projectEntity.getTasks().add(task);
        return projectEntity;
    }
}