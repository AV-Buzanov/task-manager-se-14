package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.dto.SessionDTO;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.repository.SessionRepository;
import ru.buzanov.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    @NotNull
    private ISessionRepository repository;
    @NotNull
    private EntityManager manager;

    public SessionService(ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    public void load(@Nullable final SessionDTO session) throws Exception {
        if (session == null || session.getId() == null || session.getSignature() == null)
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        repository.load(toEntity(session));
        manager.getTransaction().commit();
        manager.close();
    }

    public void load(@Nullable final List<SessionDTO> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        for (@NotNull final SessionDTO session : list)
            repository.load(toEntity(session));
        manager.getTransaction().commit();
        manager.close();
    }

    public @NotNull Collection<SessionDTO> findAll() throws Exception {
        openTransaction();
        @Nullable final List<SessionDTO> list = new ArrayList<>();
        for (@NotNull final Session session : repository.findAll())
            list.add(toDTO(session));
        manager.close();
        return list;
    }

    @Nullable
    public SessionDTO findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            return toDTO(repository.findOne(id));
        } finally {
            manager.close();
        }
    }

    public void merge(@Nullable final String id, @Nullable final SessionDTO session) throws Exception {
        if (session == null || session.getSignature() == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        repository.merge(toEntity(session));
        manager.getTransaction().commit();
        manager.close();
    }

    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        repository.remove(id);
        manager.getTransaction().commit();
        manager.close();
    }

    public void removeAll() throws Exception {
        openTransaction();
        repository.removeAll();
        manager.getTransaction().commit();
        manager.close();
    }

    private void openTransaction() {
        manager = serviceLocator.getManagerFactory().createEntityManager();
        repository = new SessionRepository(manager);
        manager.getTransaction().begin();
    }

    @Nullable
    private SessionDTO toDTO(@Nullable final Session session) {
        if (session == null)
            return null;
        @NotNull final SessionDTO sessionDto = new SessionDTO();
        if (session.getId() != null)
            sessionDto.setId(session.getId());
        if (session.getSignature() != null)
            sessionDto.setSignature(session.getSignature());
        if (session.getCreateDate() != null)
            sessionDto.setCreateDate(new Date(session.getCreateDate().getTime()));
        if (session.getUser() != null)
            sessionDto.setUserId(session.getUser().getId());
        return sessionDto;
    }

    @Nullable
    private Session toEntity(@Nullable final SessionDTO session) {
        if (session == null)
            return null;
        @NotNull final Session projectEntity = new Session();
        if (session.getId() != null)
            projectEntity.setId(session.getId());
        if (session.getSignature() != null)
            projectEntity.setSignature(session.getSignature());
        @NotNull final UserRepository userRepository = new UserRepository(manager);
        if (session.getUserId() != null)
            projectEntity.setUser(userRepository.findOne(session.getUserId()));
        if (session.getCreateDate() != null)
            projectEntity.setCreateDate(session.getCreateDate());
        return projectEntity;
    }
}