package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.api.service.ITaskService;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TaskService extends AbstractWBSService<TaskDTO> implements ITaskService {

    @NotNull
    private ITaskRepository repository;
    @NotNull
    private EntityManager manager;

    public TaskService(ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(@Nullable final String userId, @Nullable final TaskDTO task) throws Exception {
        if (task == null || task.getId() == null || task.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (isNameExist(userId, task.getName()))
            throw new Exception("This name already exist!");
        task.setUserId(userId);
        openTransaction();
        repository.load(toEntity(task));
        manager.getTransaction().commit();
        manager.close();

    }

    @Override
    public void load(@Nullable String userId, List<TaskDTO> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        for (@NotNull final TaskDTO task : list)
            load(userId, task);
    }

    @Override
    public @NotNull Collection<TaskDTO> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : repository.findAll(userId))
            list.add(toDTO(task));
        manager.close();
        return list;
    }

    @Override
    public @NotNull Collection<TaskDTO> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : repository.findByName(userId, name))
            list.add(toDTO(task));
        manager.close();
        return list;
    }

    @Override
    public @NotNull Collection<TaskDTO> findByDescription(@Nullable final String userId, @Nullable final String desc) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (desc == null || desc.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : repository.findByDescription(userId, desc))
            list.add(toDTO(task));
        manager.close();
        return list;
    }

    @Nullable
    @Override
    public TaskDTO findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            return toDTO(repository.findOne(userId, id));
        }  finally {
            manager.close();
        }
    }

    @Override
    public boolean isNameExist(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (name == null || name.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            @Nullable Task task = repository.findName(userId, name);
            return task != null;
        }  finally {
            manager.close();
        }
    }

    @Override
    public void merge(@Nullable final String userId, @Nullable final String id, @Nullable final TaskDTO task) throws Exception {
        if (task == null || task.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (isNameExist(userId, task.getName()))
            throw new Exception("This name already exist!");
        task.setUserId(userId);
        openTransaction();
        repository.merge(userId, toEntity(task));
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public @NotNull Collection<TaskDTO> findAllOrdered(@Nullable final String userId, boolean dir, @NotNull final Field field) throws Exception {
        if (userId == null || userId.isEmpty())
            return new ArrayList<>();
        if (field == null)
            return new ArrayList<>();
        openTransaction();
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : repository.findAllOrdered(userId, dir, field))
            list.add(toDTO(task));
        manager.close();
        return list;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            repository.remove(userId, id);
            manager.getTransaction().commit();
        } catch (Exception e) {
            throw new Exception("Something wrong.");
        } finally {
            manager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        repository.removeAll(userId);
        manager.getTransaction().commit();
        manager.close();
    }

    @Nullable
    public Collection<TaskDTO> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (projectId == null || projectId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @NotNull final List<TaskDTO> list = new ArrayList<>();
        for (@NotNull final Task task : repository.findByProjectId(userId, projectId))
            list.add(toDTO(task));
        manager.close();
        return list;
    }

    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (projectId == null || projectId.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        repository.removeByProjectId(userId, projectId);
        manager.getTransaction().commit();
        manager.close();
    }

    private void openTransaction() {
        manager = serviceLocator.getManagerFactory().createEntityManager();
        repository = new TaskRepository(manager);
        manager.getTransaction().begin();
    }

    @Nullable
    private TaskDTO toDTO(@Nullable final Task task) {
        if (task == null)
            return null;
        @NotNull final TaskDTO taskDto = new TaskDTO();
        if (task.getId() != null)
            taskDto.setId(task.getId());
        if (task.getName() != null)
            taskDto.setName(task.getName());
        if (task.getStartDate() != null)
            taskDto.setStartDate(task.getStartDate());
        if (task.getFinishDate() != null)
            taskDto.setFinishDate(task.getFinishDate());
        if (task.getDescription() != null)
            taskDto.setDescription(task.getDescription());
        if (task.getStatus() != null)
            taskDto.setStatus(task.getStatus());
        if (task.getCreateDate() != null)
            taskDto.setCreateDate(task.getCreateDate());
        if (task.getUser() != null)
            taskDto.setUserId(task.getUser().getId());
        if (task.getProject() != null)
            taskDto.setProjectId(task.getProject().getId());
        return taskDto;
    }

    @Nullable
    private Task toEntity(@Nullable final TaskDTO task) throws Exception {
        if (task == null)
            return null;
        @NotNull final Task taskEntity = new Task();
        if (task.getId() != null)
            taskEntity.setId(task.getId());
        if (task.getName() != null)
            taskEntity.setName(task.getName());
        if (task.getStartDate() != null)
            taskEntity.setStartDate(task.getStartDate());
        if (task.getFinishDate() != null)
            taskEntity.setFinishDate(task.getFinishDate());
        if (task.getDescription() != null)
            taskEntity.setDescription(task.getDescription());
        if (task.getStatus() != null)
            taskEntity.setStatus(task.getStatus());
        if (task.getCreateDate() != null)
            taskEntity.setCreateDate(task.getCreateDate());
        @NotNull final UserRepository userRepository = new UserRepository(manager);
        if (task.getUserId() != null)
            taskEntity.setUser(userRepository.findOne(task.getUserId()));
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(manager);
        if (task.getProjectId() != null)
            try {
                taskEntity.setProject(projectRepository.findOne(task.getUserId(), task.getProjectId()));
            } catch (NoResultException e) {
                taskEntity.setProject(null);
            }
        return taskEntity;
    }
}