package ru.buzanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Session;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.SessionRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.repository.UserRepository;
import ru.buzanov.tm.util.PasswordUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserService extends AbstractService<UserDTO> implements IUserService {

    @NotNull
    private IUserRepository repository;
    @NotNull
    private EntityManager manager;

    public UserService(ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.serviceLocator = serviceLocator;
    }

    public void load(@Nullable final UserDTO user) throws Exception {
        if (user == null || user.getId() == null || user.getLogin() == null)
            throw new Exception("Argument can't be empty or null");
        if (isLoginExist(user.getLogin()))
            throw new Exception("This login already exist!");
        user.setRoleType(RoleType.USER);
        openTransaction();
        repository.load(toEntity(user));
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public void load(@Nullable final List<UserDTO> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        for (@NotNull final UserDTO user : list)
            load(user);
    }

    @Override
    public @NotNull Collection<UserDTO> findAll() throws Exception {
        openTransaction();
        @NotNull final List<UserDTO> list = new ArrayList<>();
        for (@NotNull final User user : repository.findAll())
            list.add(toDTO(user));
        manager.close();
        return list;
    }

    @Nullable
    @Override
    public UserDTO findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        try {
            return toDTO(repository.findOne(id));
        } finally {
            manager.close();
        }
    }

    @Override
    public void merge(@Nullable final String id, @Nullable final UserDTO user) throws Exception {
        if (user == null || user.getName() == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (isLoginExist(user.getLogin()))
            throw new Exception("This login already exist!");
        user.setRoleType(RoleType.USER);
        openTransaction();
        repository.merge(toEntity(user));
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        repository.remove(id);
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public void removeAll() throws Exception {
        openTransaction();
        repository.removeAll();
        manager.getTransaction().commit();
        manager.close();
    }

    private void openTransaction() {
        manager = serviceLocator.getManagerFactory().createEntityManager();
        repository = new UserRepository(manager);
        manager.getTransaction().begin();
    }

    @Nullable
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty())
            return null;
        openTransaction();
        try {
            return toDTO(repository.findByLogin(login));
        } finally {
            manager.close();
        }
    }

    @Nullable
    public Collection<UserDTO> findByRole(@Nullable final RoleType role) throws Exception {
        if (role == null)
            return null;
        openTransaction();
        @NotNull final List<UserDTO> list = new ArrayList<>();
        for (@NotNull final User user : repository.findByRole(role))
            list.add(toDTO(user));
        manager.close();
        return list;
    }

    public boolean isPassCorrect(@Nullable final String login, @Nullable final String pass) throws Exception {
        if (login == null || login.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (pass == null || pass.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @Nullable final User user = repository.findByLogin(login);
        manager.close();
        if (user == null)
            throw new Exception("Argument can't be empty or null");
        return user.getPasswordHash().equals(PasswordUtil.hashingPass(pass, user.getId()));
    }

    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty())
            throw new Exception("Argument can't be empty or null");
        openTransaction();
        @Nullable User user;
        try {
            user = repository.findByLogin(login);
            return user != null;
        } catch (NoResultException e) {
            return false;
        } finally {
            manager.close();
        }
    }

    @Nullable
    public String getList() throws Exception {
        int indexBuf = 1;
        @NotNull final StringBuilder s = new StringBuilder();
        @NotNull final Collection<UserDTO> list = findAll();
        for (@NotNull final UserDTO user : list) {
            s.append(indexBuf).append(". ").append(user.getLogin());
            if (list.size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) throws Exception {
        if (userId == null || userId.isEmpty())
            return null;
        int indexBuf = 1;
        @NotNull final Collection<UserDTO> list = findAll();
        for (@NotNull final UserDTO entity : list) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    @Nullable
    private UserDTO toDTO(@Nullable final User user) {
        if (user == null)
            return null;
        @NotNull final UserDTO userDto = new UserDTO();
        if (user.getId() != null)
            userDto.setId(user.getId());
        if (user.getName() != null)
            userDto.setName(user.getName());
        if (user.getLogin() != null)
            userDto.setLogin(user.getLogin());
        if (user.getRoleType() != null)
            userDto.setRoleType(user.getRoleType());
        return userDto;
    }

    @Nullable
    private User toEntity(@Nullable final UserDTO user) throws Exception {
        if (user == null)
            return null;
        @NotNull final User userEntity = new User();
        if (user.getId() != null)
            userEntity.setId(user.getId());
        if (user.getLogin() != null)
            userEntity.setLogin(user.getLogin());
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty() || user.getPasswordHash().length() < 6)
            throw new Exception("Pass can't be empty or less 6 symbols");
        userEntity.setPasswordHash(PasswordUtil.hashingPass(user.getPasswordHash(), user.getId()));
        if (user.getName() != null)
            userEntity.setName(user.getName());
        if (user.getRoleType() != null)
            userEntity.setRoleType(user.getRoleType());
        @NotNull final SessionRepository sessionRepository = new SessionRepository(manager);
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(manager);
        @NotNull final TaskRepository taskRepository = new TaskRepository(manager);
        for (@NotNull final Session session : sessionRepository.findByUserId(user.getId()))
            userEntity.getSessions().add(session);
        for (@NotNull final Project project : projectRepository.findAll(user.getId()))
            userEntity.getProjects().add(project);
        for (@NotNull final Task task : taskRepository.findAll(user.getId()))
            userEntity.getTasks().add(task);
        return userEntity;
    }
}